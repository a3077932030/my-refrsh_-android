package com.ohuang.refresh;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class TextRefreshView extends FrameLayout implements RefreshView{
    private Context context;
    private TextView textView;
    private Refresh refresh;

    public TextRefreshView(@NonNull Context context) {
        this(context,null);
    }

    public TextRefreshView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TextRefreshView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.refresh_top, this, true);
        textView = view.findViewById(R.id.tv_top);
    }

    @Override
    public void initRefresh() {
        textView.setText("下拉刷新");
    }

    @Override
    public void onRefresh(Refresh refresh) {
       textView.setText("正在刷新...");
    }

    @Override
    public void refreshComplete() {

    }

    @Override
    public void onRelease(Refresh refresh) {
         textView.setText("刷新完成");
         this.refresh=refresh;

         textView.postDelayed(new Runnable() {
             @Override
             public void run() {
                 refresh.refreshComplete();
             }
         },2000);
    }


}
