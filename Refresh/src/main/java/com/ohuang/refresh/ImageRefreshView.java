package com.ohuang.refresh;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ImageRefreshView extends FrameLayout implements RefreshView {
    private Context context;
    private ImageView imageView;
    AnimationDrawable animationDrawable;
    private int animId=R.drawable.anim_load_box;



    public ImageRefreshView(@NonNull Context context) {
        this(context, null);
    }

    public ImageRefreshView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageRefreshView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.refresh_top_img, this, true);
        imageView = view.findViewById(R.id.img);
        imageView.setImageResource(animId);
        animationDrawable= (AnimationDrawable) imageView.getDrawable();
    }

    public void setAnimId(int id){
        animId=id;
        imageView.setImageResource(animId);
        animationDrawable= (AnimationDrawable) imageView.getDrawable();
    }

    public AnimationDrawable getAnimationDrawable() {
        return animationDrawable;
    }

    @Override
    public void initRefresh() {

    }

    @Override
    public void onRefresh(Refresh refresh) {
        animationDrawable.start();
    }

    @Override
    public void refreshComplete() {
       animationDrawable.stop();
    }

    @Override
    public void onRelease(Refresh refresh) {
        refresh.refreshComplete();
    }
}
