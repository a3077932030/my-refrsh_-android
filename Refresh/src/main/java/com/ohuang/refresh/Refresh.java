package com.ohuang.refresh;

public interface Refresh {
    void refreshComplete();

    void release();
}
