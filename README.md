### 引入

```
maven { url 'https://jitpack.io' }
implementation 'com.gitee.a3077932030:my-refrsh_-android:v1.0.2'
```

### 简单使用

xml代码：

```
<com.ohuang.refresh.OhRefreshLayout
    android:id="@+id/refresh"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    >
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/rv_main"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</com.ohuang.refresh.OhRefreshLayout>
```

调用

```
ohRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

    @Override
    public void onRefresh(Refresh refresh) {//下拉刷新时调用
        executorService.execute(runnable1);
    }

    @Override
    public void onBottomRefresh(Refresh refresh) {//上拉刷新调用
        executorService.execute(runnable2);
    }
});

```

刷新完成后需要调用以下代码停止刷新动画

```
ohRefreshLayout.refreshComplete();

```

禁用下拉刷新

```
ohRefreshLayout.setCanPullUp(false);//禁用上拉
ohRefreshLayout.setCanPullDown(false);//禁用下拉
```

其他使用

```
ohRefreshLayout.setMaxPullDownHeight();//最大下拉高度
ohRefreshLayout.setMaxPullUpHeight();//最大上拉高度
ohRefreshLayout.setBottomFreshViewH();//底部组件高度
ohRefreshLayout.setTopFreshViewHeight();//顶部组件高度
```

自定义刷新样式

```
public class ImageRefreshView extends FrameLayout 
implements RefreshView {//自定义View并实现RefreshView接口

     void initRefresh();//开始下拉或上拉时会调用

    void onRefresh(Refresh refresh);//刷新时调用

    void refreshComplete();//刷新完成时调用
    
    void onRelease(Refresh refresh);//使用ohRefreshLayout.release()方法时调用

}

 ohRefreshLayout.setBottomFreshView();//设置底部刷新组件
 ohRefreshLayout.setTopFreshView();//设置顶部刷新组件
```