package com.ohuang.refresh;

public interface RefreshView {
    void initRefresh();

    void onRefresh(Refresh refresh);

    void refreshComplete();

    void onRelease(Refresh refresh);


}
